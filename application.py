#! /usr/bin/env python3

from flask import Flask, render_template
from db import *
app = Flask(__name__)
app.debug = True

Gtodo={
    'bob':["Python","Manger","GIT"],
    'alice':["dormir","Manger","GIT"],
    'cliff':["Flask","Manger","GIT"],
}

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    base=DB()
    todo=base.get(name)
    return render_template("user.html",name=name,todo=todo)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
