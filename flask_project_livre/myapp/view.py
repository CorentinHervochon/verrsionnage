from .app import *
from flask import render_template, redirect
from .models import get_sample,get_author
from flask_wtf import FlaskForm
from wtforms import StringField,HiddenField,validators
from wtforms.validators import DataRequired

class AuthorForm(FlaskForm):
	id = HiddenField(id)
	name = StringField('Nom',
		[validators.InputRequired(),
		validators.Length(min=2,max=25)])

@app.route('/')
def home():
	return render_template(
		"home.html",
		title="Tiny Amazon",
		books=get_sample()
	)
@app.route("/edit/author/")
@app.route("/edit/author/<int:id>")
def edit_author(id=None):
	nom=None
	if id is None:
		a=get_author(id)
		f=AuthorForm(id=a.id, name=a.name)
	else:
		nom=a.name
		f=AuthorForm(id=a.id, name=a.name)
		return render_template("edit-author.html",author=a,form=f)

@app.route("/save/author/",methods=("POST",))
def save_author():
	f=AuthorForm()
	if f.id.data !="":
		id=int(f.id.data)
		a=get_author(id)
	else:
		a=Author(name=f.name.data)
		db.session.add(a)
		db.session.commit()
		id=a.id
	if f.validate_on_submit():
		a.name=f.name.data
		db.session.commit()
		return redirect(url_for('one_author',id=id))
	return render_template("adit-author",author=a,form=f)


# from .app import app, db
# from flask import render_template, redirect, url_for
# from .models import Author, get_sample, get_book_detail, get_author
# from flask_wtf import FlaskForm
# from wtforms import StringField, HiddenField, validators
#
#
# class AuthorForm(FlaskForm):
#         id   = HiddenField('id')
#         name = StringField('Nom',
#                 [validators.InputRequired(),
#                 validators.Length(min=2, max=25,
#                 message="Le nom doit avoir entre 2 et 25 caractères !"),
#                 #validators.Regexp('^\w+$', message="Username must contain only letters numbers or underscore"),
#                 validators.Regexp('^[a-zA-Z ]+$', message="Le nom doit contenir seulement des lettres et espaces"),
#                 ])
#
# @app.route("/")
# def home():
#     return render_template(
#     "home.html",
#     title="Tiny Amazon",
#     books=get_sample())
#
# @app.route("/author/<int:id>")
# def one_author(id):
#         auteur =  get_author(id)
#         return render_template(
#     "home.html",
#     title="Livres de "+ auteur.name,
#     books=auteur.books)
#
# @app.route("/edit/author/")
# @app.route("/edit/author/<int:id>")
# def edit_author(id=None):
#         nom = None
#         if id is not None:
#                 a = get_author(id)
#                 nom = a.name
#         else:
#                 a = None
#         f = AuthorForm(id=id,name=nom)
#         return render_template(
#                         "edit_author.html", author=a, form=f)
#
# @app.route("/save/author/", methods=["POST"])
# def save_author():
#         f = AuthorForm()
#         # Si on est en update d'Author
#         if f.id.data != "":
#                 id = int(f.id.data)
#                 a = get_author(id)
#         else:
#                 a=Author(name=f.name.data)
#                 db.session.add(a)
#         if f.validate_on_submit():
#                 a.name = f.name.data
#                 db.session.commit()
#                 id=a.id
#                 return redirect(url_for('one_author', id=id))
#         return render_template(
#                 "edit_author.html",
#                 author=a, form =f)
#
# @app.route("/detail/<bookid>")
# def detail(bookid):
#     return render_template("detail.html",
#      book=get_book_detail(bookid)
#      )
